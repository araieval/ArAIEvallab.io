---
title: "Organizers"
date: 2024-01-01T12:33:46+10:00
featured: true
weight: 7
fb: "https://facebook.com"
twi: "https://twitter.com"
lin: "https://linkdin.com"
layout: content
---

*   [Firoj Alam](https://sites.google.com/site/firojalam/), Qatar Computing Research Institute, Qatar
*   Maram Hasanain, Qatar Computing Research Institute, Qatar
*   Reem Suwaileh, HBKU, Qatar
*   Md. Arid Hasan, University of New Brunswick, Canada
*   Fatema Ahmed, Qatar Computing Research Institute, Qatar
*   Md. Rafiul Biswas, HBKU, Qatar
*   Wajdi Zaghouani, HBKU, Qatar

## Acknowledgments
The contributions of this work were funded by the NPRP grant 14C-0916-210015, which is provided by the [Qatar National Research Fund](https://qrdi.org.qa/en-us/) (a member of Qatar Foundation).

<p><img src="{{site.baseurl}}/assets/images/qrdi.png" alt="QRDI" style="float: left; margin-right: 10px;" /></p>
<br/>
<br/>
<p><img src="{{site.baseurl}}/assets/images/qnrf.png" alt="QNRF" style="float: left; margin-right: 10px;" /></p>
