---
title: "Important Dates"
date: 2024-01-01T12:33:46+10:00
featured: true
weight: 2
cat: "agile"
thumb: "../../assets/images/portfolio/portfolio_2col_6.jpg"
fb: "https://facebook.com"
twi: "https://twitter.com"
lin: "https://linkdin.com"
layout: content
---


- **15 Mar 2024:** Registration on CodaLab and the start of the development cycle (release of training and development datasets, along with submission for the development phase on CodaLab)
- **27 April 2023 23:59 AOE:** Beginning of the evaluation cycle (test sets release)
- ~~**1 May 2024 23:59 AOE:** End of the evaluation cycle (run submission)~~
- **4 May 2024 23:59 AOE:** End of the evaluation cycle (run submission)
- **5 May 2024:** Release leaderboard
- ~~**15 May 2024:** Deadline for the submission of shared task papers~~
- ~~**17 May 2024:** Deadline for the submission of shared task papers~~
- ~~**20 May 2024:** Deadline for the submission of shared task papers~~
- **22 May 2024:** Deadline for the submission of shared task papers
- **17 June 2024:** Notification of acceptance of shared task papers
- **1 July 2024:** Deadline for submission of camera-ready papers
- **16 August 2023:** [ArabicNLP Conference](https://arabicnlp2024.sigarab.org/) (colocated with [ACL-2024](https://2024.aclweb.org/))
