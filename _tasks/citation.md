---
title: "Citation"
date: 2024-01-01T12:33:46+10:00
featured: true
weight: 5
fb: "https://facebook.com"
twi: "https://twitter.com"
lin: "https://linkdin.com"
layout: content
---

**Please cite the following papers.**


<a id="1">[1]</a>
Maram Hasanain, Md. Arid Hasan, Fatema Ahmed, Reem Suwaileh, Md. Rafiul Biswas, Wajdi Zaghouani, and Firoj Alam. "ArAIEval Shared Task: Propagandistic Techniques Detection in Unimodal and Multimodal Arabic Content". In Proceedings of the Second Arabic Natural Language Processing Conference (ArabicNLP 2024), August 2024, Bangkok. Association for Computational Linguistics.

<a id="2">[2]</a>
Firoj Alam, Abul Hasnat, Fatema Ahmed, Md Arid Hasan, and Maram Hasanain. "ArMeme: Propagandistic Content in Arabic Memes." Last modified 2024. arXiv preprint arXiv:2406.03916.
<br>

<a id="3">[3]</a>
Maram Hasanain, Fatema Ahmed, and Firoj Alam. "Can GPT-4 Identify Propaganda? Annotation and Detection of Propaganda Spans in News Articles." In Proceedings of the 2024 Joint International Conference On Computational Linguistics, Language Resources And Evaluation (LREC-COLING 2024), 2024.


<a id="4">[4]</a>
Maram Hasanain and Fatema Ahmed and Firoj Alam. "Large Language Models for Propaganda Span Annotation." arXiv preprint arXiv:2311.09812 (2023).


<a id="5">[5]</a>
Maram Hasanain, Firoj Alam, Hamdy Mubarak, Samir Abdaljalil, Wajdi Zaghouani, Preslav Nakov, Giovanni Da San Martino, and Abed Alhakim Freihat. 2023. "ArAIEval Shared Task: Persuasion Techniques and Disinformation Detection in Arabic Text." In Proceedings of the First Arabic Natural Language Processing Conference (ArabicNLP 2023), December. Singapore: Association for Computational Linguistics.


<a id="6">[6]</a>
Firoj Alam, Hamdy Mubarak, Wajdi Zaghouani, Giovanni Da San Martino, and Preslav Nakov. 2022. "Overview of the WANLP 2022 Shared Task on Propaganda Detection in Arabic.", In Proceedings of the Seventh Arabic Natural Language Processing Workshop (WANLP), 108–118, December. Abu Dhabi, United Arab Emirates (Hybrid): Association for Computational Linguistics. [https://aclanthology.org/2022.wanlp-1.11](https://aclanthology.org/2022.wanlp-1.11).


```
@inproceedings{araieval:arabicnlp2024-overview,
    title = ArAIEval Shared Task: Propagandistic Techniques Detection in Unimodal and Multimodal Arabic Content,
    author = "Hasanain, Maram and Hasan, Md. Arid and Ahmed, Fatema and Suwaileh, Reem  and Biswas, Md. Rafiul and Zaghouani, Wajdi and Alam, Firoj",
    booktitle = "Proceedings of the Second Arabic Natural Language Processing Conference (ArabicNLP 2024)",
    month = Aug,
    year = "2024",
    address = "Bangkok",
    publisher = "Association for Computational Linguistics",
}

@article{alam2024armeme,
  title={ArMeme: Propagandistic Content in Arabic Memes},
  author={Alam, Firoj and Hasnat, Abul and Ahmed, Fatema and Hasan, Md Arid and Hasanain, Maram},
  year={2024},
  journal={arXiv preprint arXiv:2311.09812},
}

@inproceedings{hasanain2024can,
  title={Can GPT-4 Identify Propaganda? Annotation and Detection of Propaganda Spans in News Articles},
  author={Hasanain, Maram and Ahmed, Fatema and Alam, Firoj},
  booktitle = "Proceedings of the 2024 Joint International Conference On Computational Linguistics, Language Resources And Evaluation",
  series = "LREC-COLING 2024",
  year={2024},
  address={Torino, Italy}
}

@article{hasanain2023large,
  title={Large Language Models for Propaganda Span Annotation},
  author={Hasanain, Maram and Ahmed, Fatema and Alam, Firoj},
  journal={arXiv preprint arXiv:2311.09812},
  year={2023}
}

@inproceedings{araieval:arabicnlp2023-overview,
    title = "ArAIEval Shared Task: Persuasion Techniques and Disinformation Detection in Arabic Text",
    author = "Hasanain, Maram and Alam, Firoj and Mubarak, Hamdy, and Abdaljalil, Samir  and Zaghouani, Wajdi and Nakov, Preslav  and Da San Martino, Giovanni and Freihat, Abed Alhakim",
    booktitle = "Proceedings of the First Arabic Natural Language Processing Conference (ArabicNLP 2023)",
    month = Dec,
    year = "2023",
    address = "Singapore",
    publisher = "Association for Computational Linguistics",
}

@inproceedings{alam-etal-2022-overview,
    title = "Overview of the {WANLP} 2022 Shared Task on Propaganda Detection in {A}rabic",
    author = "Alam, Firoj  and
      Mubarak, Hamdy  and
      Zaghouani, Wajdi  and
      Da San Martino, Giovanni  and
      Nakov, Preslav",
    booktitle = "Proceedings of the The Seventh Arabic Natural Language Processing Workshop (WANLP)",
    month = dec,
    year = "2022",
    address = "Abu Dhabi, United Arab Emirates (Hybrid)",
    publisher = "Association for Computational Linguistics",
    url = "https://aclanthology.org/2022.wanlp-1.11",
    pages = "108--118",
}
```
