---
title: "Proceedings"
date: 2024-01-01T12:33:46+10:00
featured: true
weight: 4
fb: "https://facebook.com"
twi: "https://twitter.com"
lin: "https://linkdin.com"
layout: content
---

#### Instructions to Prepare Your Paper:
------------------------------------------------------
**The title of paper should be in the following format:**
\<Team Name\> at ArAIEval Shared Task: \<Title of the work\>

***For example:***
Scholarly at ArAIEval Shared Task: LLMs in Persuasion Technique Detection for Identifying Manipulative Strategies

**Cite:** Please check the citation section to cite the shared task papers

#### Paper Submission Instructions and Link:
------------------------------------------------------
 **Templates:** Please find the templates at: [https://github.com/acl-org/acl-style-files/](https://github.com/acl-org/acl-style-files/)

**Paper Length:**
Shared task description papers may include up to 4 pages of content, in addition to unlimited references.

**More information:** For more details, please visit the following link: [https://arabicnlp2024.sigarab.org/call-for-papers](https://arabicnlp2024.sigarab.org/call-for-papers)

**Paper submission:** [https://openreview.net/group?id=SIGARAB.org/ArabicNLP/2024/Shared_Task/ArAIEval](https://openreview.net/group?id=SIGARAB.org/ArabicNLP/2024/Shared_Task/ArAIEval)

#### Suggested structure for the system description papers:
------------------------------------------------------
 **Abstract:** four/five sentences highlighting your approach and key results. <br>
**Introduction:** 3/4 a page expanding on the abstract mentioning key background such as why the task is challenging for current modeling techniques and why your approach is interesting/novel.<br>
**Related Work:** This section provides a review of existing research in the field, with a focus on highlighting how our contribution diverges from and adds value to the current body of work.<br>
**Data:** review of the data you used to train your system. Be sure to mention the size of the training, validation and test sets that you’ve used, and the label distributions, as well as any tools you used for preprocessing data.<br>
**System:** a detailed description of how the systems were built and trained. If you’re using a neural network, were there pre-trained embeddings, how was the model trained, what hyperparameters were chosen and experimented with? How long did the model take to train, and on what infrastructure? Linking to source code is valuable here as well, but the description should be able to stand alone as a full description of how to reimplement the system. While other paper styles include background as a separate section, it’s fine to simply include citations to similar systems which inspired your work as you describe your system.<br>
**Results:** a description of the key results of the paper. If you have done extra error analysis into what types of errors the system makes, this is extremely valuable for the reader. Unofficial results from after the submission deadline can be very useful as well.<br>
**Discussion:** general discussion of the task and your system. Description of characteristic errors and their frequency over a sample of development data. What would you do if you had another 3 months to work on it?<br>
**Conclusion:** a restatement of the introduction, highlighting what was learned about the task and how to model it.
