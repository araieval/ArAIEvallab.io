---
title: "Contact"
date: 2024-01-01T12:33:46+10:00
featured: true
weight: 6
fb: "https://facebook.com"
twi: "https://twitter.com"
lin: "https://linkdin.com"
layout: content
---


Slack: [channel](https://join.slack.com/t/araieval/shared_invite/zt-20rzypxs7-LuHUsw8ltj7ylae9c4I7XQ)  
Email: [araieval@googlegroups.com](mailto:araieval@googlegroups.com)
