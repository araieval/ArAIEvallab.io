---
title: "Tasks"
date: 2024-01-01T12:33:46+10:00
featured: true
weight: 1
cat: "agile"
thumb: "../../assets/images/portfolio/portfolio_2col_6.jpg"
fb: "https://facebook.com"
twi: "https://twitter.com"
lin: "https://linkdin.com"
layout: content
---

- [Task 1:](task1) Unimodal (Text) Propagandistic Technique Detection
- [Task 2:](task2) Multimodal Propagandistic Memes Classification
