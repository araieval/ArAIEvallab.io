---
title: Araieval - Arabic AI Tasks Evaluation (ArAIEval)
layout: content
description: Arabic AI Tasks Evaluation (ArAIEval)
# intro_image: "assets/images/hero-img.png"
intro_image_absolute: true
intro_image_hide_on_mobile: true
---
# Welcome to ArAIEval shared task at ArabicNLP 2024!
Propagandistic content spreading online in mainstream and social media is often used to mislead the general audience. Automatic identification of such content is crucial to assist fact-checkers, journalists, and the general audience in challenging the information presented in this content.

**ArAIEval** offers two shared tasks:
1. detection of propagandistic textual spans with persuasion techniques identification (unimodal),
2. distinguishing between propagandistic and non-propagandistic memes (multimodal).

**ArAIEval** covers both the tweets, news articles and memes. The classification settings includes span identification and binary subtasks.


{% include task-card.html %}
