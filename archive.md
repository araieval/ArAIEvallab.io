---
title: 'Previous Edition of the Shared Tasks'
date: 2012-01-20T17:01:34+07:00
layout: page
# banner_image: "../assets/images/banner-shape.png"
bodyClass: page-archive
---

# 2023

## Propaganda and disinformation detection in Arabic

- [Website](https://araieval.gitlab.io/2023/)


<br>
<br>
<br>

# 2022

## Propaganda Detection in Arabic

- [Website](https://sites.google.com/view/propaganda-detection-in-arabic/)
- [Data](https://gitlab.com/araieval/propaganda-detection/)
